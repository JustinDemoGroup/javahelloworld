# This file is a template, and might need editing before it works on your project.
#FROM maven:3.5-jdk-11 as BUILD

#COPY . /src/main/java/com/example/javamavenjunithelloworld
#RUN mvn clean update

FROM openjdk:11-jdk
EXPOSE 8080
#COPY --from=BUILD /usr/src/app/target /opt/target
COPY target/*.jar helloworld.jar
#WORKDIR /opt/target

CMD ["javac","JavaHelloWorld"]
